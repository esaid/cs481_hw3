import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData.light(),
      darkTheme: ThemeData.dark(), 
      title: 'My Cool App',
      home: HomePage()
      );
   }
  }

  Column buttonColumn(Color color, IconData icon, String label){
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [ Icon(icon, color: color), Container( margin: const EdgeInsets.only(top: 8),
          child: Text( label, style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400,
              color: color,
            ),
          ),
          ),
      ],
    );

    
  }


class StarWidget extends StatefulWidget {
  @override 
  StarWidgetState createState() => StarWidgetState();
}

 class StarWidgetState extends State<StarWidget> {
   bool starred = true;
   int rating = 0;
   @override 
   Widget build(BuildContext context) {
     return Row(mainAxisSize: MainAxisSize.min, children: [
       Container(padding: EdgeInsets.all(0), 
       child: IconButton(
         icon: starred ? Icon(Icons.star_border) : Icon(Icons.star), 
         color: Colors.yellow[500], 
         onPressed: toggleStar,
         ),
       ),
       SizedBox(
         width: 20,
         child: Container(
           child: Text('$rating'),
         )
       )
     ],
     );
   } 
   void toggleStar() {
     setState(() {
       if(starred) {
         starred = false;
         rating += 1;
       }
       else {
         starred = true;
         rating -= 1;
       }
     });
   }
 }

 class HomePage extends StatefulWidget {
   @override
   HomePageState createState() => HomePageState();
 }

 class HomePageState extends State<HomePage> {
   @override 
   Widget build(BuildContext context) {

Widget title = Container(padding: const EdgeInsets.all(32), child: Row(
      children: [ Expanded(child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: const EdgeInsets.only(bottom: 8),
            child: Text( 'GeForce RTX 3080', style: TextStyle(fontWeight: FontWeight.bold,),
            ),
          ),
          Text('Next-generation ray tracing', style: TextStyle(color: Colors.grey[500],),
          )
        ],
      ),
      ),
      StarWidget(), //Stateful
      ],
    )
    );




   Color color = Theme.of(context).primaryColor;
    Widget buttonSection = Container(
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        buttonColumn(color, Icons.memory, """
        8704 CUDA Cores 
        1.71GHZ 
        10GB GDDR6X"""),
        buttonColumn(color, Icons.shopping_cart, 'out of stock'),
      ],
    )
  );

 Widget textDescription = Container(
  padding: const EdgeInsets.all(32),
  child: Text(
    'Twice the performance of the RTX 2080! '
    'Featuring second generation ray tracing cores and third generation tensor cores. '
    'Good luck buying this graphic card because the bots bought them all! '
    'Might be lucky to obtain one before the end of the year. ',
    softWrap: true,
  ),
);

     return Scaffold(
        appBar: AppBar(
          title: Text('GeForce RTX'),
          actions: <Widget> [
           IconButton(icon: Icon(Icons.settings), onPressed: () {
             print('hello');
             Navigator.of(context).push(MaterialPageRoute(builder: (context) => SettingsRoute() ));
           } 
           )
           ]
        ), 
        body: ListView(
          children: [
            Image.asset('images/rtx3080.webp',
            width: 600,
            height: 240,
            fit: BoxFit.fitWidth,
            ),
            title,
            buttonSection,
            textDescription
          ]
          ),
        );
   }
 }


 class SettingsRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Settings"),
      ),
      body: Center(
        child: RaisedButton(
          onPressed: () {
            Navigator.pop(context);// Navigate back to first route when tapped.
          },
          child: Text('Go back!'),
        ),
      ),
    );
  }
}